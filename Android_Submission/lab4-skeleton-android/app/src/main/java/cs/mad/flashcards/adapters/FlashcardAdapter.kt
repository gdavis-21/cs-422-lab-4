package cs.mad.flashcards.adapters

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.activities.MainActivity
import cs.mad.flashcards.entities.Flashcard
import java.util.zip.Inflater

class FlashcardAdapter(dataSet: List<Flashcard>) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = dataSet.toMutableList()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question
        viewHolder.itemView.setOnClickListener {
            AlertDialog.Builder(viewHolder.itemView.context)
                .setTitle(dataSet.get(viewHolder.adapterPosition).question)
                .setMessage(dataSet.get(viewHolder.adapterPosition).answer)
                .setPositiveButton("Edit") { dialogInterface: DialogInterface, i: Int ->
                    // Open a dialog to edit the flashcard question and answer.
                    val titleView = View.inflate(viewHolder.itemView.context, R.layout.custom_title, null)
                    val bodyView = View.inflate(viewHolder.itemView.context, R.layout.custom_body, null)
                    val titleEditText = titleView.findViewById<EditText>(R.id.custom_title)
                    val bodyEditText = bodyView.findViewById<EditText>(R.id.custom_body)
                    titleEditText.setText(dataSet.get(viewHolder.adapterPosition).question)
                    bodyEditText.setText(dataSet.get(viewHolder.adapterPosition).answer)
                    AlertDialog.Builder(viewHolder.itemView.context)
                        .setCustomTitle(titleView)
                        .setView(bodyView)
                        .setPositiveButton("Done") {dialogInterface: DialogInterface, i: Int ->
                            dataSet.get(viewHolder.adapterPosition).question = titleEditText.text.toString()
                            dataSet.get(viewHolder.adapterPosition).answer = bodyEditText.text.toString()
                            notifyDataSetChanged()
                        }
                        .setNegativeButton("Delete") {dialogInterface: DialogInterface, i: Int ->
                            dataSet.removeAt(viewHolder.adapterPosition)
                            notifyDataSetChanged()
                        }
                        .create()
                        .show()
                }
                .setNegativeButton("Close") {dialogInterface: DialogInterface, i: Int ->
                    dialogInterface.dismiss()
                }
                .create()
                .show()
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }
}