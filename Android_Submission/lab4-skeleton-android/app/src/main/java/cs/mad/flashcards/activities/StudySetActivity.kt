package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import cs.mad.flashcards.R
import cs.mad.flashcards.databinding.ActivityStudySetBinding
import cs.mad.flashcards.entities.Flashcard

class StudySetActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStudySetBinding
    var flashcards = Flashcard.getHardcodedFlashcards() as MutableList

    var countCorrect: Int = 0
    var countMissed: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_study_set)
        findViewById<TextView>(R.id.flashcard_text).text = flashcards[0].question

        findViewById<Button>(R.id.back_button).setOnClickListener {
            finish()
        }

        findViewById<Button>(R.id.button_missed).setOnClickListener {
            flashcards[0].missed = true
            countMissed += 1;
            findViewById<TextView>(R.id.missed_count).text = "Missed: " + (countMissed).toString()
            flashcards = moveFirstToEnd(flashcards)
            findViewById<TextView>(R.id.flashcard_text).text = flashcards[0].question
        }

        findViewById<Button>(R.id.button_skip).setOnClickListener {
            flashcards = moveFirstToEnd(flashcards)
            findViewById<TextView>(R.id.flashcard_text).text = flashcards[0].question
        }

        findViewById<Button>(R.id.button_correct).setOnClickListener {
            if (!flashcards[0].missed) {
                countCorrect += 1
                findViewById<TextView>(R.id.correct_count).text = "Correct: " + (countCorrect).toString()
            }
            flashcards.removeAt(0)
            findViewById<TextView>(R.id.flashcard_text).text = flashcards[0].question
        }

        findViewById<TextView>(R.id.flashcard_text).setOnClickListener {
            findViewById<TextView>(R.id.flashcard_text).text = flashcards[0].answer
        }
    }

    fun moveFirstToEnd(arr: MutableList<Flashcard>): MutableList<Flashcard> {
        var arrCopy = arr
        val temp: Flashcard = arrCopy[0]
        for (index in 1..arrCopy.size - 1) {
            arrCopy[index - 1] = arrCopy[index]
        }
        arrCopy[arrCopy.size - 1] = temp
        return arrCopy
    }
}