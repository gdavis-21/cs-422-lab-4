//
//  CustomAlertViewController.swift
//  CS422L
//
//  Created by Grant Davis on 2/14/22.
//

import UIKit

class CustomAlertViewController: UIViewController {
    var parentVC: FlashCardSetDetailViewController!
    @IBOutlet var alertView: UIView!
    @IBOutlet var term: UITextField!
    @IBOutlet var definition: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupToLookPretty()
        term.text = parentVC.cards[parentVC.selectedIndex].term
        definition.text = parentVC.cards[parentVC.selectedIndex].definition
    }
    
    @IBAction func save(_ sender: Any) {
        // Chance term/definition to new text and tell tableView to reload.
        parentVC.cards[parentVC.selectedIndex].term = term.text ?? ""
        parentVC.cards[parentVC.selectedIndex].definition = definition.text ?? ""
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    @IBAction func deleteCard(_ sender: Any) {
        parentVC.cards.remove(at: parentVC.selectedIndex)
        parentVC.tableView.reloadData()
        self.dismiss(animated: true, completion: {})
    }
    
    func setupToLookPretty()
    {
        alertView.layer.cornerRadius = 8.0
        alertView.layer.borderWidth = 3.0
        alertView.layer.borderColor = UIColor.gray.cgColor
        term.becomeFirstResponder()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
