//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Grant Davis on 2/15/22.
//

import UIKit

class StudySetViewController: UIViewController {
    
    @IBOutlet var studyView: UIView!
    @IBOutlet var term: UILabel!
    @IBOutlet var correct: UILabel!
    @IBOutlet var missed: UILabel!
    @IBOutlet var flashcard: UIView!
    var missedCount: Int = 0
    var correctCount: Int = 0
    
    
    var flashcards = Flashcard.getHardCodedCollection()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        term.text = flashcards[0].term
    }
    @IBAction func missedButton(_ sender: Any) {
        flashcards[0].missed = true
        moveFirstToEnd(arr: &flashcards)
        missedCount += 1
        missed.text = "Missed: \(missedCount)"
        term.text = flashcards[0].term
        }

    @IBAction func tapFlashcard(_ sender: Any) {
        term.text = flashcards[0].definition
    }
    
    @IBAction func skippedButton(_ sender: Any) {
        moveFirstToEnd(arr: &flashcards)
        term.text = flashcards[0].term
    }
    
    @IBAction func correctButton(_ sender: Any) {
        if (flashcards[0].missed == false) {
            correctCount += 1
            correct.text = "Correct: \(correctCount)"
        }
        flashcards.remove(at: 0)
        term.text = flashcards[0].term
    }
    
    func moveFirstToEnd(arr: inout [Flashcard]) {
        let temp: Flashcard = arr[0]
        for index in 1...arr.count - 1 {
            arr[index - 1] = arr[index]
        }
        arr[arr.count - 1] = temp
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
